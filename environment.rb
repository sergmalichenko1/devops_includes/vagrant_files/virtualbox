PROJECT_NAME = "APP" # Prefix for every VM 

SSH_PUB_KEYS = [
  File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip, # user key
  # pubkey2,
  # pubkey3
]

ENVIRONMENTS = {
  "STAGE" => [ # Prefix for VM from this array
    {
      "name" => "app",
      "amount" => 1,
      "network" => "192.168.56.", # /24 subnet
      "address_start_from" => 2,  # VM addresses in the subnet {network}.{address_start_from} - {network}.{address_start_from + amount - 1}
      "box" => "ubuntu/focal64",
      "cpus" => 1,
      "memory" => 1024,
      "provisions" => [
        # relative path 1,
        # relative path 2
      ]
    },
    {
      "name" => "db",
      "amount" => 1,
      "network" => "192.168.56.", # /24 subnet
      "address_start_from" => 11,  # VM addresses in the subnet {network}.{address_start_from} - {network}.{address_start_from + amount - 1}
      "box" => "ubuntu/focal64",
      "cpus" => 1,
      "memory" => 1024,
      "provisions" => [
        # relative path 1,
        # relative path 2
      ]
    }
  ],

  "PROD" => [ # Prefix for VM from this array
    {
      "name" => "app",
      "amount" => 1,
      "network" => "192.168.57.", # /24 subnet
      "address_start_from" => 2,  # VM addresses in the subnet {network}.{address_start_from} - {network}.{address_start_from + amount - 1}
      "box" => "ubuntu/focal64",
      "cpus" => 1,
      "memory" => 1024,
      "provisions" => [
        # relative path 1,
        # relative path 2
      ]
    }
  ]
}
