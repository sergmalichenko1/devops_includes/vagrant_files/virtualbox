require_relative 'environment.rb'

Vagrant.configure("2") do |config|

  def create_vm(config, box, hostname, ip, provisions, cpus=1, memory=1024)
    config.vm.define hostname do |host|
      host.vm.box = box
      host.vm.network "private_network", ip: ip
      host.vm.hostname = hostname

      SSH_PUB_KEYS.each do |key|
        host.vm.provision "shell", inline: <<-SHELL
          echo #{key} >> /home/vagrant/.ssh/authorized_keys
          echo #{key} >> /root/.ssh/authorized_keys
        SHELL
      end

      provisions.each do |path|
        host.vm.provision "shell", path: path
      end

      host.vm.provider "virtualbox" do |vm|
        vm.name = "#{PROJECT_NAME}-#{hostname}"
        vm.customize ["modifyvm", :id, "--cpus", cpus.to_s]
        vm.customize ["modifyvm", :id, "--memory", memory.to_s]
        vm.customize ["modifyvm", :id, "--audio", "none"]
      end
    end
  end

  ENVIRONMENTS.each do |environment, machines|
    machines.each do |machine|
      (1..machine["amount"]).each do |i|
        create_vm(
          config=config,
          box=machine["box"],
          hostname="#{environment}-#{machine["name"]}#{"%02d" % i}",
          ip="#{machine["network"]}#{machine["address_start_from"] + i - 1}",
          provisions=machine["provisions"],
          cpus=machine["cpus"],
          memory=machine["memory"]
        )
      end
    end
  end
end

